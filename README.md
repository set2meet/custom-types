# Description

This project for defining your own types. All types are located in separate directories in one common directory "types".

## types/nightwatch

Contains types for the Nightwatch API (see https://nightwatchjs.org/).
The Nightwatch project is capable of working with asynchronous functions using callbacks and Promises. Existing type definitions (see https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/nightwatch/index.d.ts) are not fully suitable for tasks where the async / await syntax is used. This implementation includes replacing regular objects with Promise objects, which we can use in projects where we need to work with Promises.