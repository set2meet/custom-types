declare module 'testing-platform-nightwatch' {
  import {
    NightwatchBrowser,
    NightwatchAPI,
    NightwatchGlobals,
    NightwatchAssertions
  } from 'nightwatch';

  export interface NightwatchCustomGlobals {
    taskId: string;
    profileName: string;
    browserType: string;
    browserVersion: string;
    participantName: string;
    scriptPath: string;
    nodeAgentUrl: string;
    websocketPort: number;
    [key: string]: number | string | object;
  }

  export enum EVIdeoStatus {
    ON = 'on',
    OFF = 'off',
    PAUSE = 'pause',
  }


  /* Metric models */

  export interface ISysMetric {
    cpu: number;
    memory: number;
  }

  export interface IMetrics {
    sys: ISysMetric;
    rtc: IRTCMetric;
  }

  export interface IMediaGroup {
    [name: string]: IIncomingMedia;
  }

  export interface IRTCMetric {
    self: IOutgoingMedia; // outgoing streams of the current participant
    participants: IMediaGroup; // incoming streams from remote participants
    unknown?: IIncomingMedia; // streams come from unknows source
  }

  export interface IOutgoingMedia {
    audio: IOutputAudioStream[];
    video: IOutputVideoStream[];
  }

  export interface IIncomingMedia {
    audio: IInputAudioStream[];
    video: IInputVideoStream[];
  }

  export interface IOutputStream {
    ssrc: number;
    bytesSent: IValueSummary;
    bitrate: IValueSummary;
    packetsSent: IValueSummary;
    packetsLost: IValueSummary;
    jitter: IValueSummary;
    roundTripTime: IValueSummary;
  }

  export type IOutputAudioStream = IOutputStream;

  export interface IOutputVideoStream extends IOutputStream {
    framesEncoded: IValueSummary;
    framesSent: IValueSummary;
    framesPerSecond: IValueSummary;
    frameWidth: IValueSummary;
    frameHeight: IValueSummary;
  }

  export interface IInputStream {
    ssrc: number;
    bytesReceived: IValueSummary;
    bitrate: IValueSummary;
    packetsReceived: IValueSummary;
    packetsLost: IValueSummary;
  }

  export interface IInputAudioStream extends IInputStream {
    jitter: IValueSummary;
  }

  export interface IInputVideoStream extends IInputStream {
    framesReceived: IValueSummary;
    framesDecoded: IValueSummary;
    framesDropped: IValueSummary;
    framesPerSecond: IValueSummary;
    frameWidth: IValueSummary;
    frameHeight: IValueSummary;
  }

  export interface IValueSummary {
    max: number;
    min: number;
    average: number;
    deviation: number;
    violation: number;
  }

  export interface IRange {
    min: number;
    max: number;
  }

  export interface IOutputStreamSettings {
    bytesSent?: IRange;
    bitrate?: IRange;
    packetsSent?: IRange;
    packetsLost?: IRange;
    jitter?: IRange;
    roundTripTime?: IRange;
  }

  export type IOutputAudioStreamSettings = IOutputStreamSettings;

  export interface IOutputVideoStreamSettings extends IOutputStreamSettings {
    framesEncoded?: IRange;
    framesSent?: IRange;
    framesPerSecond?: IRange;
    frameWidth?: IRange;
    frameHeight?: IRange;
  }

  export interface IInputStreamSettings {
    bytesReceived?: IRange;
    bitrate?: IRange;
    packetsReceived?: IRange;
    packetsLost?: IRange;
  }

  export interface IInputAudioStreamSettings extends IInputStreamSettings {
    jitter?: IRange;
  }

  export interface IInputVideoStreamSettings extends IInputStreamSettings {
    framesReceived?: IRange;
    framesDecoded?: IRange;
    framesDropped?: IRange;
    framesPerSecond?: IRange;
    frameWidth?: IRange;
    frameHeight?: IRange;
  }

  export interface IOutputMediaSettings {
    audio?: IOutputAudioStreamSettings;
    video?: IOutputVideoStreamSettings;
  }

  export interface IInputMediaSettings {
    audio?: IInputAudioStreamSettings;
    video?: IInputVideoStreamSettings;
  }

  export interface IMediaSettings {
    output?: IOutputMediaSettings;
    input?: IInputMediaSettings;
  }

  export interface IParticipantSettings {
    [key: string]: IInputMediaSettings;
  }

  export interface IConvertationSettings {
    general?: IMediaSettings;
    participants?: IParticipantSettings;
  }







  export interface IRTCSettings {
    general?: IMediaSettings;
    participants?: IParticipantSettings;
  }

  export interface ISysSettings {
    cpu?: number;
    memory?: number;
  }

  export interface ICompareSettings {
    sys?: ISysSettings;
    rtc?: IRTCSettings;
  }

  export interface ISysResult {
    cpu?: string;
    memory?: string;
  }

  export interface IOutgoingMediaResult {
    audio: IOutputAudioStreamResult[];
    video: IOutputVideoStreamResult[];
  }

  export interface IIncomingMediaResult {
    audio: IInputAudioStreamResult[];
    video: IInputVideoStreamResult[];
  }

  export interface IMediaGroupResult {
    [name: string]: IIncomingMediaResult;
  }

  export interface IRTCResult {
    self: IOutgoingMediaResult;
    participants: IMediaGroupResult;
  }

  export interface ICompareResult {
    error: boolean;
    message: string;
    sys?: ISysResult;
    rtc?: IRTCResult;
  }
  export interface IOutputStreamResult {
    bytesSent: string;
    bitrate: string;
    packetsSent: string;
    packetsLost: string;
    jitter: string;
    roundTripTime: string;
  }

  export type IOutputAudioStreamResult = IOutputStreamResult;

  export interface IOutputVideoStreamResult extends IOutputStreamResult {
    framesEncoded: string;
    framesSent: string;
    framesPerSecond: string;
    frameWidth: string;
    frameHeight: string;
  }

  export interface IInputStreamResult {
    bytesReceived: string;
    bitrate: string;
    packetsReceived: string;
    packetsLost: string;
  }

  export interface IInputAudioStreamResult extends IInputStreamResult {
    jitter: string;
  }

  export interface IInputVideoStreamResult extends IInputStreamResult {
    framesReceived: string;
    framesDecoded: string;
    framesDropped: string;
    framesPerSecond: string;
    frameWidth: string;
    frameHeight: string;
  }


  /* END Metric models */

  export interface NightwatchCustomAssertions {
    video(selector: string, attempts: number): Promise<void>;
  }

  export interface CustomSyncCommands {
    getSyncValue(pointName: string, timeout: number): Promise<any>;
    setSyncValue(pointName: string, value: any): Promise<void>;
    syncBarrier(pointName: string, timeout: number): Promise<void>;
  }

  export interface CustomVideoCommands {
    getVideoHashcode(selector: string): Promise<number>;
    getVideoStatus(selector: string): Promise<EVIdeoStatus>;
  }

  export interface CustomMetricCommands {
    initMetrics(): Promise<void>;
    resetMetrics(): Promise<void>;
    getMetrics(settings?: IConvertationSettings, reset?: boolean): Promise<IMetrics>;
    saveMetrics(name: string, metrics: IMetrics): Promise<void>;
    loadMetrics(name: string): Promise<IMetrics>;
    compareMetrics(oldMetrics: IMetrics, newMetrics: IMetrics, settings?: ICompareSettings): Promise<ICompareResult>;
  }

  export interface CustomNightwatchBrowser extends NightwatchBrowser, CustomSyncCommands, CustomVideoCommands, CustomMetricCommands {
    globals: NightwatchCustomGlobals & NightwatchGlobals;
    assert: NightwatchAssertions & NightwatchCustomAssertions;
    verify: NightwatchAssertions & NightwatchCustomAssertions;
  }
}
